public class Curso {
    private String nome;
    private String sigla;
    private Departamento depto;

    public Curso(String nome, String sigla, Departamento depto) {
        this.nome = nome;
        this.sigla = sigla;
        this.depto = depto;
    }

    @Override
    public String toString() {
        return "Curso:\n" +
                "\t\tnome= " + nome + '\n' +
                "\t\tsigla=" + sigla + '\n' +
                "\t\tdepto=" + depto.toString() + '\n';
    }
}
