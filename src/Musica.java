import java.util.ArrayList;
import java.util.List;

public class Musica {

    private String nome;
    private int ano;
    private String tipo;
    private List<Compositor> compositores = new ArrayList<Compositor>();

    public Musica(String nome, int ano, String tipo){
        this.nome = nome;
        this.ano = ano;
        this.tipo = tipo;

    }

    public Musica(String nome, int ano, String tipo, List<Compositor> compositores) {
        this.nome = nome;
        this.ano = ano;
        this.tipo = tipo;
        this.compositores = compositores;
    }

    public String toString(){
        String musicaToString = "Nome da música: " + this.getNome() + "\nAno: " + this.getAno() + "\n";
        musicaToString += "Compositores da música: \n";
        for(Compositor compositor : compositores){
            musicaToString += "      " +compositor.getNome()+"\n";
        }
        return musicaToString;
    }

    public String getNome(){
        return nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public int getAno(){
        return ano;
    }

    public void setAno(int ano){
        this.ano = ano;
    }

    public void addCompositor(Compositor compositor) {
        this.compositores.add(compositor);
    }

    public List<Compositor> getCompositores(){
        return this.compositores;
    }

}