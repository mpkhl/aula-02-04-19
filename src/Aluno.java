public class Aluno {
    private String nome;
    private int matricula;
    private int ano;
    private Curso curso;

    @Override
    public String toString() {
        return "Aluno:\n" +
                "\tnome= " + nome + '\n' +
                "\tmatricula= " + matricula + '\n' +
                "\tano=" + ano + '\n' +
                "\t" + curso.toString() + '\n';
    }

    public Aluno(String nome, int matricula, int ano, Curso curso) {
        this.nome = nome;
        this.matricula = matricula;
        this.ano = ano;
        this.curso = curso;
    }
}
