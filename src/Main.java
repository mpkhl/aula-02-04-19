import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Compositor caetano = new Compositor("Caetano Veloso", "Brasileiro");
        Compositor chico = new Compositor("Chico Buarque de Holanda", "Brasileiro");
        Compositor sivuca = new Compositor("Severino Dias de Oliveira", "Brasileiro");

        Musica cotidiano = new Musica("Cotidiano", 1986, "Bossa Nova", Arrays.asList(caetano, chico));
        Musica joaoEMaria = new Musica("João e Maria", 1977, "Bossa Nova", Arrays.asList(sivuca, chico));


        System.out.println(cotidiano.toString());
        System.out.println(joaoEMaria.toString());

        Departamento inf = new Departamento("Instituto de Informática", "INF");
        Curso es = new Curso("Engenharia de Software", "ES", inf);
        Aluno mikael = new Aluno("Mikael Brenner", 10001, 2017, es);

        System.out.println(mikael.toString());

        Endereco endereco = new Endereco("Rua Juca", 1, "Setor Sul", "-", "Goiânia", "Goiás");
        Endereco endereco2 = new Endereco("Rua Banana", 210, "Setor Central", "Qd 20 Lt 5", "Goiânia", "Goiás");
        Cliente pedrinho = new Cliente("Little Pedro", "000.000.000-00", endereco);
        Cliente aninha = new Cliente("Little Ana", "111.111.111-11", endereco2);
        System.out.println(pedrinho.toString());
        System.out.println(aninha.toString());

        Endereco enderecoEmpresa = new Endereco("Rua 87", 184, "Setor Sul", "-", "Goiânia", "Goiás");
        Empresa bolinhaCorp = new Empresa("0000.0000.0000-00", "Bolinha Corporations", enderecoEmpresa);
        Empregado juca = new Empregado("Juca Chaves", 0001, bolinhaCorp);

        System.out.println(juca);
    }

}

