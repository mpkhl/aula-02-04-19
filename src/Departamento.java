public class Departamento {
    public Departamento(String nome, String sigla) {
        this.nome = nome;
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    private String nome;
    private String sigla;

    @Override
    public String toString() {
        return "\n" +
                "\t\t\tnome=" + nome + '\n' +
                "\t\t\tsigla=" + sigla + '\n';
    }
}
